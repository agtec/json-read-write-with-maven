import java.io.*;

public class CopyLines {

    public void copyLine() throws IOException {

        BufferedReader in = null;
        PrintWriter out = null;

        try {
            in = new BufferedReader(new FileReader("plikTekstowy.txt"));
            out = new PrintWriter(new FileWriter("linesOutput.txt"));

            String s;

            while ((s = in.readLine()) != null) {
                out.println(s);
            }
        } finally {
            if (in != null) {
                in.close();
            }

            if (out != null) {
                out.close();
            }
        }
    }
}
