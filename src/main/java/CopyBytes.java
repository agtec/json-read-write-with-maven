import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class CopyBytes {

    public void copyByte() {

        FileInputStream in = null;
        FileOutputStream out = null;

        try {
            in = new FileInputStream("plikTekstowy.txt");
            out = new FileOutputStream("out.txt");

            int c;

            while ((c = in.read()) != -1) {
                out.write(c);
                System.out.println(c);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
