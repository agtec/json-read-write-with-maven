import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;

public class StudentToJSONParser {

    public void zapis(Student student) {

        JSONObject obj = new JSONObject();
        obj.put("name", student.getName());
        obj.put("lastname", student.getLastname());
        obj.put("age", student.getAge());

        JSONArray list = new JSONArray();
        list.addAll(student.getListOfStudentSubjects());
        /*list.add("math");
        list.add("biology");
        list.add("chemistry");*/

        obj.put("subjects", list);

        try (FileWriter file2 = new FileWriter(student.getName())) {

            file2.write(obj.toJSONString());
            file2.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.print(obj);
    }


    public void load() throws FileNotFoundException {
        JSONParser parser = new JSONParser();

        try {

            Object obj2 = parser.parse(new FileReader("Zapis"));

            JSONObject jsonObject = (JSONObject) obj2;
            System.out.println(jsonObject);

            String name = (String) jsonObject.get("name");
            System.out.println(name);

            Long age = (Long) jsonObject.get("age");
            System.out.println(age);

            // loop array
            JSONArray msg = (JSONArray) jsonObject.get("subjects");
            Iterator<String> iterator = msg.iterator();
            while (iterator.hasNext()) {
                System.out.println(iterator.next());
            }
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
