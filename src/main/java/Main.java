import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException {

        CopyBytes streams = new CopyBytes();
        streams.copyByte();

        CopyCharacters cs = new CopyCharacters();
        cs.copyChar();

        CopyLines cl = new CopyLines();
        cl.copyLine();

        List<String> subjects = new ArrayList<String>();
        subjects.add("math");
        subjects.add("biology");

        Student student1 = new Student("ABC", "DEF", 20, subjects);
        Student student2 = new Student("GHI", "JKL", 21, subjects);

        StudentToJSONParser stjp = new StudentToJSONParser();

        stjp.zapis(student1);
        stjp.load();
        stjp.zapis(student2);
        stjp.load();

    }
}
