import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class CopyCharacters {

    public void copyChar() {

        FileReader in = null;
        FileWriter out = null;

        try {
            in = new FileReader("plikTekstowy2.txt");
            out = new FileWriter("charactersOutput.txt");

            int c;

            while ((c = in.read()) != -1) {
                out.write(c);
                System.out.println(c);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
