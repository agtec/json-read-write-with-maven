import java.util.List;

public class Student {

    private String name;
    private String lastname;
    private int age;
    private List<String> listOfStudentSubjects;

    public Student(String name, String lastname, int age, List<String> listOfStudentSubjects) {
        this.name = name;
        this.lastname = lastname;
        this.age = age;
        this.listOfStudentSubjects = listOfStudentSubjects;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public List<String> getListOfStudentSubjects() {
        return listOfStudentSubjects;
    }

    public void setListOfStudentSubjects(List<String> listOfStudentSubjects) {
        this.listOfStudentSubjects = listOfStudentSubjects;
    }



}
